/* Copyright 2015 Dave Astels.  All rights reserved. */
/* Use of this source code is governed by a BSD-style */
/* license that can be found in the LICENSE file. */

/* This package implements a basic LISP interpretor for the ARM Cortex M4 */
/* This file contains tests for list functions. */

#include <string.h>
#include "unity.h"
#include "parser.h"
#include "tokenizer.h"
#include "data.h"
#include "dictionary.h"
#include "hash.h"
#include "header.h"
#include "vector.h"
#include "utils.h"
#include "evaluator.h"
#include "environment_frame.h"
#include "environment_vector.h"
#include "primitive_function.h"
#include "function.h"
#include "primitives.h"
#include "special_forms.h"

char *err;

void setUp(void)
{
  err = NULL;
  initialize_lisp_data_system();
  initialize_environment();
  register_special_forms();
  register_primitives();
}


void test_integer_less_than_true(void)
{
  TEST_ASSERT_TRUE(boolean_value(parse_and_eval("(< 2 3)", &err)));
}


void test_integer_less_than_false(void)
{
  TEST_ASSERT_FALSE(boolean_value(parse_and_eval("(< 3 2)", &err)));
}


void test_integer_less_than_when_equal(void)
{
  TEST_ASSERT_FALSE(boolean_value(parse_and_eval("(< 3 3)", &err)));
}



void test_unsignned_less_than_true(void)
{
  TEST_ASSERT_TRUE(boolean_value(parse_and_eval("(< #x02 #x03)", &err)));
}


void test_unsigned_less_than_false(void)
{
  TEST_ASSERT_FALSE(boolean_value(parse_and_eval("(< #x03 #x02)", &err)));
}


void test_unsigned_less_than_when_equal(void)
{
  TEST_ASSERT_FALSE(boolean_value(parse_and_eval("(< #x03 #x03)", &err)));
}


void test_integer_less_than_equal_true(void)
{
  TEST_ASSERT_TRUE(boolean_value(parse_and_eval("(<= 2 3)", &err)));
}


void test_integer_less_than_equal_false(void)
{
  TEST_ASSERT_FALSE(boolean_value(parse_and_eval("(<= 3 2)", &err)));
}


void test_integer_less_than_equal_when_equal(void)
{
  TEST_ASSERT_TRUE(boolean_value(parse_and_eval("(<= 3 3)", &err)));
}



void test_unsignned_less_than_equal_true(void)
{
  TEST_ASSERT_TRUE(boolean_value(parse_and_eval("(<= #x02 #x03)", &err)));
}


void test_unsigned_less_than_equal_false(void)
{
  TEST_ASSERT_FALSE(boolean_value(parse_and_eval("(<= #x03 #x02)", &err)));
}


void test_unsigned_less_than_equal_when_equal(void)
{
  TEST_ASSERT_TRUE(boolean_value(parse_and_eval("(<= #x03 #x03)", &err)));
}


void test_integer_greater_than_equal_true(void)
{
  TEST_ASSERT_TRUE(boolean_value(parse_and_eval("(>= 3 2)", &err)));
}


void test_integer_greater_than_equal_false(void)
{
  TEST_ASSERT_FALSE(boolean_value(parse_and_eval("(>= 2 3)", &err)));
}


void test_integer_greater_than_equal_when_equal(void)
{
  TEST_ASSERT_TRUE(boolean_value(parse_and_eval("(>= 3 3)", &err)));
}



void test_unsignned_greater_than_equal_true(void)
{
  TEST_ASSERT_TRUE(boolean_value(parse_and_eval("(>= #x03 #x02)", &err)));
}


void test_unsigned_greater_than_equal_false(void)
{
  TEST_ASSERT_FALSE(boolean_value(parse_and_eval("(>= #x02 #x03)", &err)));
}


void test_unsigned_greater_than_equal_when_equal(void)
{
  TEST_ASSERT_TRUE(boolean_value(parse_and_eval("(>= #x03 #x03)", &err)));
}


void test_integer_greater_than_true(void)
{
  TEST_ASSERT_TRUE(boolean_value(parse_and_eval("(> 3 2)", &err)));
}


void test_integer_greater_than_false(void)
{
  TEST_ASSERT_FALSE(boolean_value(parse_and_eval("(> 2 3)", &err)));
}


void test_integer_greater_than_when_equal(void)
{
  TEST_ASSERT_FALSE(boolean_value(parse_and_eval("(> 3 3)", &err)));
}



void test_unsignned_greater_than_true(void)
{
  TEST_ASSERT_TRUE(boolean_value(parse_and_eval("(> #x03 #x02)", &err)));
}


void test_unsigned_greater_than_false(void)
{
  TEST_ASSERT_FALSE(boolean_value(parse_and_eval("(> #x02 #x03)", &err)));
}


void test_unsigned_greater_than_when_equal(void)
{
  TEST_ASSERT_FALSE(boolean_value(parse_and_eval("(> #x03 #x03)", &err)));
}
