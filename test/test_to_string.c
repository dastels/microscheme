/* Copyright 2015 Dave Astels.  All rights reserved. */
/* Use of this source code is governed by a BSD-style */
/* license that can be found in the LICENSE file. */

/* This package implements a basic LISP interpretor for the ARM Cortex M4 */
/* This file contains tests for functions. */

#include <string.h>
#include "unity.h"
#include "parser.h"
#include "tokenizer.h"
#include "data.h"
#include "dictionary.h"
#include "hash.h"
#include "header.h"
#include "vector.h"
#include "utils.h"
#include "evaluator.h"
#include "environment_frame.h"
#include "environment_vector.h"
#include "primitive_function.h"
#include "function.h"
#include "special_forms.h"

void setUp(void)
{
  initialize_lisp_data_system();
  initialize_environment();
  register_special_forms();
}

void test_integer_to_string(void)
{
  TEST_ASSERT_EQUAL_STRING("42", to_string(integer_with_value(42)));
}


void test_unsigned_integer_to_string(void)
{
  TEST_ASSERT_EQUAL_STRING("#x00000055", to_string(unsigned_integer_with_value((__uint32_t)0x55)));
}


void test_true_to_string(void)
{
  TEST_ASSERT_EQUAL_STRING("#t", to_string(LISP_TRUE));
}


void test_false_to_string(void)
{
  TEST_ASSERT_EQUAL_STRING("#f", to_string(LISP_FALSE));
}


void test_string_to_string(void)
{
  TEST_ASSERT_EQUAL_STRING("\"hello\"", to_string(string_with_value("hello")));
}


void test_symbol_to_string(void)
{
  TEST_ASSERT_EQUAL_STRING("hello", to_string(intern_symbol("hello")));
}


void test_lambda_to_string(void)
{
  char *err = NULL;
  data_t *lambda = parse_and_eval("(lambda () 42)", &err);
  TEST_ASSERT_EQUAL_STRING("<func: anonymous>", to_string(lambda));
}


void test_function_to_string(void)
{
  char *err = NULL;
  data_t *f = parse_and_eval("(define (f x) x)", &err);
  TEST_ASSERT_EQUAL_STRING("<func: f>", to_string(f));
}


data_t *add_impl(data_t *args, environment_frame_t *env, char **err_ptr)
{
  data_t *arg1 = car(args);
  data_t *arg2 = car(cdr(args));
  return integer_with_value(integer_value(arg1) + integer_value(arg2));
}


void test_prim_to_string(void)
{
  char *err = NULL;
  register_primitive("+", 2, &add_impl);
  data_t *f = value_of(GLOBAL_ENV, intern_symbol("+"));
  TEST_ASSERT_EQUAL_STRING("<prim: +>", to_string(f));
}


void test_empty_list_to_string(void)
{
  TEST_ASSERT_EQUAL_STRING("()", to_string(NULL));
}


void test_singleton_list_to_string(void)
{
  char *err = NULL;
  data_t *l = parse("(42)", &err);
  TEST_ASSERT_EQUAL_STRING("(42)", to_string(l));
}


void test_multi_item_list_to_string(void)
{
  char *err = NULL;
  data_t *l = parse("(1 2 3 \"hi\")", &err);
  TEST_ASSERT_EQUAL_STRING("(1 2 3 \"hi\")", to_string(l));
}


void test_nested_list_to_string(void)
{
  char *err = NULL;
  data_t *l = parse("(1 (2 3) 4)", &err);
  TEST_ASSERT_EQUAL_STRING("(1 (2 3) 4)", to_string(l));
}
