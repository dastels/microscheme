/* Copyright 2015 Dave Astels.  All rights reserved. */
/* Use of this source code is governed by a BSD-style */
/* license that can be found in the LICENSE file. */

/* This package implements a basic LISP interpretor for the ARM Cortex M4 */
/* This file contains tests for the parser. */

#include "unity.h"
#include "parser.h"
#include "tokenizer.h"
#include "data.h"
#include "dictionary.h"
#include "hash.h"
#include "header.h"
#include "vector.h"
#include "utils.h"
#include "evaluator.h"
#include "environment_frame.h"
#include "environment_vector.h"

void setUp(void)
{
  initialize_lisp_data_system();
}

/* Simple data */

void test_parsing_integer(void) {
  char *err = NULL;
  data_t *sexpr = parse("5", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(sexpr);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(sexpr));
  TEST_ASSERT_EQUAL_INT(5, integer_value(sexpr));
}


void test_parsing_another_integer(void) {
  char *err = NULL;
  data_t *sexpr = parse("476", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(sexpr);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(sexpr));
  TEST_ASSERT_EQUAL_INT(476, integer_value(sexpr));
}


void test_parsing_negative_integer(void) {
  char *err = NULL;
  data_t *sexpr = parse("-5", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(sexpr);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(sexpr));
  TEST_ASSERT_EQUAL_INT(-5, integer_value(sexpr));
}


void test_parsing_hex_integer(void) {
  char *err = NULL;
  data_t *sexpr = parse("#xa5", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(sexpr);
  TEST_ASSERT_EQUAL_INT(UNSIGNED_INTEGER_TYPE, type_of(sexpr));
  TEST_ASSERT_EQUAL_INT(0xa5, integer_value(sexpr));
}


void test_parsing_uppercase_hex_integer(void) {
  char *err = NULL;
  data_t *sexpr = parse("#xA5", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(sexpr);
  TEST_ASSERT_EQUAL_INT(UNSIGNED_INTEGER_TYPE, type_of(sexpr));
  TEST_ASSERT_EQUAL_INT(0xa5, integer_value(sexpr));
}


void test_parsing_mixed_case_hex_integer(void) {
  char *err = NULL;
  data_t *sexpr = parse("#xAe", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(sexpr);
  TEST_ASSERT_EQUAL_INT(UNSIGNED_INTEGER_TYPE, type_of(sexpr));
  TEST_ASSERT_EQUAL_INT(0xae, integer_value(sexpr));
}


void test_parsing_string(void) {
  char *err = NULL;
  data_t *sexpr = parse("\"test\"", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(sexpr);
  TEST_ASSERT_EQUAL_INT(STRING_TYPE, type_of(sexpr));
  TEST_ASSERT_EQUAL_STRING("test", string_value(sexpr));
}


void test_parsing_another_string(void) {
  char *err = NULL;
  data_t *sexpr = parse("\"Lots of Stylish Parentheses\"", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(sexpr);
  TEST_ASSERT_EQUAL_INT(STRING_TYPE, type_of(sexpr));
  TEST_ASSERT_EQUAL_STRING("Lots of Stylish Parentheses", string_value(sexpr));
}


void test_parsing_boolean_true(void) {
  char *err = NULL;
  data_t *sexpr = parse("#t", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(sexpr);
  TEST_ASSERT_EQUAL_INT(BOOLEAN_TYPE, type_of(sexpr));
  TEST_ASSERT_TRUE(boolean_value(sexpr));
}


void test_parsing_boolean_false(void) {
  char *err = NULL;
  data_t *sexpr = parse("#f", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(sexpr);
  TEST_ASSERT_EQUAL_INT(BOOLEAN_TYPE, type_of(sexpr));
  TEST_ASSERT_FALSE(boolean_value(sexpr));
}


void test_parsing_symbol(void) {
  char *err = NULL;
  data_t *sexpr = parse("test", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(sexpr);
  TEST_ASSERT_EQUAL_INT(SYMBOL_TYPE, type_of(sexpr));
  TEST_ASSERT_EQUAL_STRING("test", string_value(sexpr));
}


void test_parsing_another_symbol(void) {
  char *err = NULL;
  data_t *sexpr = parse("another", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(sexpr);
  TEST_ASSERT_EQUAL_INT(SYMBOL_TYPE, type_of(sexpr));
  TEST_ASSERT_EQUAL_STRING("another", string_value(sexpr));
}


void test_parsing_symbol_uniqueness(void) {
  char *err = NULL;
  data_t *sym1 = parse("test", &err);
  data_t *sym2 = parse("test", &err);
  TEST_ASSERT_EQUAL_PTR(sym1, sym2);
}


void test_parsing_symbol_with_underscores(void) {
  char *err = NULL;
  data_t *sexpr = parse("_test_1", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(sexpr);
  TEST_ASSERT_EQUAL_INT(SYMBOL_TYPE, type_of(sexpr));
  TEST_ASSERT_EQUAL_STRING("_test_1", string_value(sexpr));
}

/* Complex data */


void test_parsing_empty_list(void) {
  char *err = NULL;
  data_t *sexpr = parse("()", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NULL(sexpr);
}


void test_parsing_integer_car(void) {
  char *err = NULL;
  data_t *sexpr = parse("(1)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(sexpr);
  TEST_ASSERT_EQUAL_INT(CONS_CELL_TYPE, type_of(sexpr));
  TEST_ASSERT_EQUAL_INT(1, length_of(sexpr));
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(car(sexpr)));
  TEST_ASSERT_EQUAL_INT(1, integer_value(car(sexpr)));
}


void test_parsing_string_car(void) {
  char *err = NULL;
  data_t *sexpr = parse("(\"hi\")", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(sexpr);
  TEST_ASSERT_EQUAL_INT(CONS_CELL_TYPE, type_of(sexpr));
  TEST_ASSERT_EQUAL_INT(1, length_of(sexpr));
  TEST_ASSERT_EQUAL_INT(STRING_TYPE, type_of(car(sexpr)));
  TEST_ASSERT_EQUAL_STRING("hi", string_value(car(sexpr)));
}


void test_multi_item_list(void) {
  char *err = NULL;
  data_t *sexpr = parse("(\"hi\" 42)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(sexpr);
  TEST_ASSERT_EQUAL_INT(CONS_CELL_TYPE, type_of(sexpr));
  TEST_ASSERT_EQUAL_INT(2, length_of(sexpr));
  TEST_ASSERT_EQUAL_INT(STRING_TYPE, type_of(car(sexpr)));
  TEST_ASSERT_EQUAL_STRING("hi", string_value(car(sexpr)));
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(car(cdr(sexpr))));
  TEST_ASSERT_EQUAL_INT(42, integer_value(car(cdr(sexpr))));
}


void test_nested_list(void) {
  char *err = NULL;
  data_t *sexpr = parse("(1 (2 3) 4)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(sexpr);
  TEST_ASSERT_EQUAL_INT(CONS_CELL_TYPE, type_of(sexpr));
  TEST_ASSERT_EQUAL_INT(3, length_of(sexpr));

  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(car(sexpr)));
  TEST_ASSERT_EQUAL_INT(1, integer_value(car(sexpr)));

  data_t *sublist = car(cdr(sexpr));
  TEST_ASSERT_EQUAL_INT(CONS_CELL_TYPE, type_of(sublist));
  TEST_ASSERT_EQUAL_INT(2, length_of(sublist));
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(car(sublist)));
  TEST_ASSERT_EQUAL_INT(2, integer_value(car(sublist)));
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(car(cdr(sublist))));
  TEST_ASSERT_EQUAL_INT(3, integer_value(car(cdr(sublist))));

  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(car(cdr(cdr(sexpr)))));
  TEST_ASSERT_EQUAL_INT(4, integer_value(car(cdr(cdr(sexpr)))));
}
