/* Copyright 2015 Dave Astels.  All rights reserved. */
/* Use of this source code is governed by a BSD-style */
/* license that can be found in the LICENSE file. */

/* This package implements a basic LISP interpretor for the ARM Cortex M4 */
/* This file contains tests for primitives. */

#include <string.h>
#include "unity.h"
#include "parser.h"
#include "tokenizer.h"
#include "data.h"
#include "dictionary.h"
#include "hash.h"
#include "header.h"
#include "vector.h"
#include "utils.h"
#include "evaluator.h"
#include "environment_frame.h"
#include "environment_vector.h"
#include "primitive_function.h"
#include "function.h"
#include "primitives.h"
#include "special_forms.h"

void setUp(void)
{
  initialize_lisp_data_system();
  initialize_environment();
  register_special_forms();
  register_primitives();
}


void test_and_with_no_args(void)
{
  char *err;
  data_t *result = parse_and_eval("(and)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(BOOLEAN_TYPE, type_of(result));
  TEST_ASSERT_TRUE(boolean_value(result));
}


void test_and_with_one_false_arg(void)
{
  char *err;
  data_t *result = parse_and_eval("(and #f)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(BOOLEAN_TYPE, type_of(result));
  TEST_ASSERT_FALSE(boolean_value(result));
}


void test_and_with_one_true_arg(void)
{
  char *err;
  data_t *result = parse_and_eval("(and #t)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(BOOLEAN_TYPE, type_of(result));
  TEST_ASSERT_TRUE(boolean_value(result));
}


void test_and_with_two_args(void)
{
  char *err;
  TEST_ASSERT_FALSE(boolean_value(parse_and_eval("(and #f #f)", &err)));
  TEST_ASSERT_FALSE(boolean_value(parse_and_eval("(and #f #t)", &err)));
  TEST_ASSERT_FALSE(boolean_value(parse_and_eval("(and #t #f)", &err)));
  TEST_ASSERT_TRUE(boolean_value(parse_and_eval(" (and #t #t)", &err)));
}


void test_or_with_no_args(void)
{
  char *err;
  data_t *result = parse_and_eval("(or)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(BOOLEAN_TYPE, type_of(result));
  TEST_ASSERT_FALSE(boolean_value(result));
}


void test_or_with_one_false_arg(void)
{
  char *err;
  data_t *result = parse_and_eval("(or #f)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(BOOLEAN_TYPE, type_of(result));
  TEST_ASSERT_FALSE(boolean_value(result));
}


void test_or_with_one_true_arg(void)
{
  char *err;
  data_t *result = parse_and_eval("(or #t)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(BOOLEAN_TYPE, type_of(result));
  TEST_ASSERT_TRUE(boolean_value(result));
}


void test_or_with_two_args(void)
{
  char *err;
  TEST_ASSERT_FALSE(boolean_value(parse_and_eval("(or #f #f)", &err)));
  TEST_ASSERT_TRUE(boolean_value(parse_and_eval(" (or #f #t)", &err)));
  TEST_ASSERT_TRUE(boolean_value(parse_and_eval(" (or #t #f)", &err)));
  TEST_ASSERT_TRUE(boolean_value(parse_and_eval(" (or #t #t)", &err)));
}


void test_not_true(void){
  char *err;
  data_t *result = parse_and_eval("(not #t)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(BOOLEAN_TYPE, type_of(result));
  TEST_ASSERT_FALSE(boolean_value(result));
}


void test_not_false(void){
  char *err;
  data_t *result = parse_and_eval("(not #f)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(BOOLEAN_TYPE, type_of(result));
  TEST_ASSERT_TRUE(boolean_value(result));
}
