/* Copyright 2015 Dave Astels.  All rights reserved. */
/* Use of this source code is governed by a BSD-style */
/* license that can be found in the LICENSE file. */

/* This package implements a basic LISP interpretor for the ARM Cortex M4 */
/* This file contains tests for list functions. */

#include <string.h>
#include "unity.h"
#include "parser.h"
#include "tokenizer.h"
#include "data.h"
#include "dictionary.h"
#include "hash.h"
#include "header.h"
#include "vector.h"
#include "utils.h"
#include "evaluator.h"
#include "environment_frame.h"
#include "environment_vector.h"
#include "primitive_function.h"
#include "function.h"
#include "primitives.h"
#include "special_forms.h"

char *err;

void setUp(void)
{
  err = NULL;
  initialize_lisp_data_system();
  initialize_environment();
  register_special_forms();
  register_primitives();
}


void test_list(void)
{
  data_t *v = parse_and_eval("(list 1 2 3)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(v);
  TEST_ASSERT_EQUAL_INT(CONS_CELL_TYPE, type_of(v));
  TEST_ASSERT_EQUAL_STRING("(1 2 3)", to_string(v));
}


void test_car(void)
{
  data_t *v = parse_and_eval("(car '(1 2 3))", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(v);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(v));
  TEST_ASSERT_EQUAL_INT(1, integer_value(v));
}


void test_cdr(void)
{
  data_t *v = parse_and_eval("(cdr '(1 2 3))", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(v);
  TEST_ASSERT_EQUAL_STRING("(2 3)", to_string(v));
}


void test_caar(void)
{
  TEST_ASSERT_EQUAL_STRING("1", to_string(parse_and_eval("(caar '((1 2) 3))", &err)));
}


void test_cadr(void)
{
  TEST_ASSERT_EQUAL_STRING("2", to_string(parse_and_eval("(cadr '(1 2 3))", &err)));
}


void test_cdar(void)
{
  TEST_ASSERT_EQUAL_STRING("(2)", to_string(parse_and_eval("(cdar '((1 2) 3))", &err)));
}


void test_cddr(void)
{
  TEST_ASSERT_EQUAL_STRING("(3)", to_string(parse_and_eval("(cddr '(1 2 3))", &err)));
}


void test_listref_0(void)
{
  TEST_ASSERT_EQUAL_STRING("1", to_string(parse_and_eval("(list-ref '(1 2 3) 0)", &err)));
  TEST_ASSERT_NULL(err);
}  


void test_listref_1(void)
{
  TEST_ASSERT_EQUAL_STRING("2", to_string(parse_and_eval("(list-ref '(1 2 3) 1)", &err)));
  TEST_ASSERT_NULL(err);
}  


void test_listref_2(void)
{
  TEST_ASSERT_EQUAL_STRING("3", to_string(parse_and_eval("(list-ref '(1 2 3) 2)", &err)));
  TEST_ASSERT_NULL(err);
}  


void test_listref_out_of_bounds(void)
{
  TEST_ASSERT_NULL(parse_and_eval("(list-ref '(1 2 3) 3)", &err));
  TEST_ASSERT_NOT_NULL(err);
}  


void test_listref_negative_index(void)
{
  TEST_ASSERT_NULL(parse_and_eval("(list-ref '(1 2 3) -1)", &err));
  TEST_ASSERT_NOT_NULL(err);
}  


void test_first(void)
{
  TEST_ASSERT_EQUAL_STRING("1", to_string(parse_and_eval("(first '(1 2 3 4 5 6 7 8 9 10))", &err)));
  TEST_ASSERT_NULL(err);
}  


void test_second(void)
{
  TEST_ASSERT_EQUAL_STRING("2", to_string(parse_and_eval("(second '(1 2 3 4 5 6 7 8 9 10))", &err)));
  TEST_ASSERT_NULL(err);
}  


void test_third(void)
{
  TEST_ASSERT_EQUAL_STRING("3", to_string(parse_and_eval("(third '(1 2 3 4 5 6 7 8 9 10))", &err)));
  TEST_ASSERT_NULL(err);
}  


void test_fourth(void)
{
  TEST_ASSERT_EQUAL_STRING("4", to_string(parse_and_eval("(fourth '(1 2 3 4 5 6 7 8 9 10))", &err)));
  TEST_ASSERT_NULL(err);
}  


void test_fifth(void)
{
  TEST_ASSERT_EQUAL_STRING("5", to_string(parse_and_eval("(fifth '(1 2 3 4 5 6 7 8 9 10))", &err)));
  TEST_ASSERT_NULL(err);
}  


void test_sixth(void)
{
  TEST_ASSERT_EQUAL_STRING("6", to_string(parse_and_eval("(sixth '(1 2 3 4 5 6 7 8 9 10))", &err)));
  TEST_ASSERT_NULL(err);
}  


void test_seventh(void)
{
  TEST_ASSERT_EQUAL_STRING("7", to_string(parse_and_eval("(seventh '(1 2 3 4 5 6 7 8 9 10))", &err)));
  TEST_ASSERT_NULL(err);
}  


void test_eigth(void)
{
  TEST_ASSERT_EQUAL_STRING("8", to_string(parse_and_eval("(eigth '(1 2 3 4 5 6 7 8 9 10))", &err)));
  TEST_ASSERT_NULL(err);
}  


void test_ninth(void)
{
  TEST_ASSERT_EQUAL_STRING("9", to_string(parse_and_eval("(ninth '(1 2 3 4 5 6 7 8 9 10))", &err)));
  TEST_ASSERT_NULL(err);
}  


void test_tenth(void)
{
  TEST_ASSERT_EQUAL_STRING("10", to_string(parse_and_eval("(tenth '(1 2 3 4 5 6 7 8 9 10))", &err)));
  TEST_ASSERT_NULL(err);
}  


void test_list_head_0(void)
{
  TEST_ASSERT_EQUAL_STRING("()", to_string(parse_and_eval("(list-head '(1 2 3 4) 0)", &err)));
  TEST_ASSERT_NULL(err);
}  


void test_list_head_1(void)
{
  TEST_ASSERT_EQUAL_STRING("(1)", to_string(parse_and_eval("(list-head '(1 2 3 4) 1)", &err)));
  TEST_ASSERT_NULL(err);
}


void test_list_head_2(void)
{
  TEST_ASSERT_EQUAL_STRING("(1 2)", to_string(parse_and_eval("(list-head '(1 2 3 4) 2)", &err)));
  TEST_ASSERT_NULL(err);
}


void test_list_head_3(void)
{
  TEST_ASSERT_EQUAL_STRING("(1 2 3)", to_string(parse_and_eval("(list-head '(1 2 3 4) 3)", &err)));
  TEST_ASSERT_NULL(err);
}


void test_list_head_4(void)
{
  TEST_ASSERT_EQUAL_STRING("(1 2 3 4)", to_string(parse_and_eval("(list-head '(1 2 3 4) 4)", &err)));
  TEST_ASSERT_NULL(err);
}


void test_list_head_with_negative_count(void)
{
  TEST_ASSERT_NULL(parse_and_eval("(list-head '(1 2 3 4) -1)", &err));
  TEST_ASSERT_NOT_NULL(err);
}


void test_list_head_with_out_of_bounds_count(void)
{
  TEST_ASSERT_NULL(parse_and_eval("(list-head '(1 2 3 4) 5)", &err));
  TEST_ASSERT_NOT_NULL(err);
}


void test_list_tail_0(void)
{
  TEST_ASSERT_EQUAL_STRING("(1 2 3 4)", to_string(parse_and_eval("(list-tail '(1 2 3 4) 0)", &err)));
  TEST_ASSERT_NULL(err);
}  


void test_list_tail_1(void)
{
  TEST_ASSERT_EQUAL_STRING("(2 3 4)", to_string(parse_and_eval("(list-tail '(1 2 3 4) 1)", &err)));
  TEST_ASSERT_NULL(err);
}


void test_list_tail_2(void)
{
  TEST_ASSERT_EQUAL_STRING("(3 4)", to_string(parse_and_eval("(list-tail '(1 2 3 4) 2)", &err)));
  TEST_ASSERT_NULL(err);
}


void test_list_tail_3(void)
{
  TEST_ASSERT_EQUAL_STRING("(4)", to_string(parse_and_eval("(list-tail '(1 2 3 4) 3)", &err)));
  TEST_ASSERT_NULL(err);
}


void test_list_tail_4(void)
{
  TEST_ASSERT_EQUAL_STRING("()", to_string(parse_and_eval("(list-tail '(1 2 3 4) 4)", &err)));
  TEST_ASSERT_NULL(err);
}


void test_list_tail_with_negative_count(void)
{
  TEST_ASSERT_NULL(parse_and_eval("(list-tail '(1 2 3 4) -1)", &err));
  TEST_ASSERT_NOT_NULL(err);
}


void test_list_tail_with_out_of_bounds_count(void)
{
  TEST_ASSERT_NULL(parse_and_eval("(list-tail '(1 2 3 4) 5)", &err));
  TEST_ASSERT_NOT_NULL(err);
}


void test_append_no_args(void)
{
  TEST_ASSERT_EQUAL_STRING("()", to_string(parse_and_eval("(append)", &err)));
  TEST_ASSERT_NULL(err);
}


void test_append_one_arg(void)
{
  TEST_ASSERT_EQUAL_STRING("(1 2 3)", to_string(parse_and_eval("(append '(1 2 3))", &err)));
  TEST_ASSERT_NULL(err);
}


void test_append_two_args(void)
{
  TEST_ASSERT_EQUAL_STRING("(1 2 3 4 5)", to_string(parse_and_eval("(append '(1 2 3) '(4 5))", &err)));
  TEST_ASSERT_NULL(err);
}


void test_append_three_args(void)
{
  TEST_ASSERT_EQUAL_STRING("(1 2 3 4 5 6 7)", to_string(parse_and_eval("(append '(1 2 3) '(4 5) '(6 7))", &err)));
  TEST_ASSERT_NULL(err);
}


void test_append_four_args(void)
{
  TEST_ASSERT_EQUAL_STRING("(1 2 3 4 5 6 7)", to_string(parse_and_eval("(append '(1 2) '(3) '(4 5) '(6 7))", &err)));
  TEST_ASSERT_NULL(err);
}


void test_append_copies(void)
{
  data_t *result = parse_and_eval_all("(define a '(1 2)) (eq? a (append a '(3 4)))", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(BOOLEAN_TYPE, type_of(result));
  TEST_ASSERT_FALSE(boolean_value(result));
}


void test_appendbang_no_args(void)
{
  TEST_ASSERT_EQUAL_STRING("()", to_string(parse_and_eval("(append!)", &err)));
  TEST_ASSERT_NULL(err);
}


void test_appendbang_one_arg(void)
{
  TEST_ASSERT_EQUAL_STRING("(1 2 3)", to_string(parse_and_eval("(append! '(1 2 3))", &err)));
  TEST_ASSERT_NULL(err);
}


void test_appendbang_two_args(void)
{
  TEST_ASSERT_EQUAL_STRING("(1 2 3 4 5)", to_string(parse_and_eval("(append! '(1 2 3) '(4 5))", &err)));
  TEST_ASSERT_NULL(err);
}


void test_appendbang_three_args(void)
{
  TEST_ASSERT_EQUAL_STRING("(1 2 3 4 5 6 7)", to_string(parse_and_eval("(append! '(1 2 3) '(4 5) '(6 7))", &err)));
  TEST_ASSERT_NULL(err);
}


void test_appendbang_four_args(void)
{
  TEST_ASSERT_EQUAL_STRING("(1 2 3 4 5 6 7)", to_string(parse_and_eval("(append! '(1 2) '(3) '(4 5) '(6 7))", &err)));
  TEST_ASSERT_NULL(err);
}


void test_appendbang_with_empty_list(void)
{
  TEST_ASSERT_EQUAL_STRING("(1 2 3 6 7)", to_string(parse_and_eval("(append! '(1 2 3) '() '(6 7))", &err)));
  TEST_ASSERT_NULL(err);
}


void test_appendbang_does_not_copy(void)
{
  data_t *result = parse_and_eval_all("(define a '(1 2)) (eq? a (append! a '(3 4)))", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(BOOLEAN_TYPE, type_of(result));
  TEST_ASSERT_TRUE(boolean_value(result));
}


