/* Copyright 2015 Dave Astels.  All rights reserved. */
/* Use of this source code is governed by a BSD-style */
/* license that can be found in the LICENSE file. */

/* This package implements a basic LISP interpretor for the ARM Cortex M4 */
/* This file contains tests for primitives. */

#include <string.h>
#include "unity.h"
#include "parser.h"
#include "tokenizer.h"
#include "data.h"
#include "dictionary.h"
#include "hash.h"
#include "header.h"
#include "vector.h"
#include "utils.h"
#include "evaluator.h"
#include "environment_frame.h"
#include "environment_vector.h"
#include "primitive_function.h"
#include "function.h"
#include "primitives.h"
#include "special_forms.h"

char *err;

void setUp(void)
{
  initialize_lisp_data_system();
  initialize_environment();
  register_special_forms();
  register_primitives();
}


void test_add_zero_args(void)
{
  data_t *result = parse_and_eval("(+)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_INT(0, integer_value(result));
 }


void test_add_one_args(void)
{
  data_t *result = parse_and_eval("(+ 1)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_INT(1, integer_value(result));
}


void test_add_two_args(void)
{
  data_t *result = parse_and_eval("(+ 1 3)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_INT(4, integer_value(result));
}


void test_add_many_args(void)
{
  data_t *result = parse_and_eval("(+ 1 3 4 5 6)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_INT(19, integer_value(result));
}


void test_multiply_zero_args(void)
{
  data_t *result = parse_and_eval("(*)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_INT(1, integer_value(result));
}


void test_multiply_one_args(void)
{
  data_t *result = parse_and_eval("(* 2)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_INT(2, integer_value(result));
}


void test_multiply_two_args(void)
{
  data_t *result = parse_and_eval("(* 2 3)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_INT(6, integer_value(result));
}


void test_multiply_many_args(void)
{
  data_t *result = parse_and_eval("(* 2 3 4 5 6)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_INT(720, integer_value(result));
}


void test_subtract_zero_args(void)
{
  data_t *result = parse_and_eval("(-)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_INT(0, integer_value(result));
}


void test_subtract_one_args(void)
{
  data_t *result = parse_and_eval("(- 2)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_INT(-2, integer_value(result));
}


void test_subtract_two_args(void)
{
  data_t *result = parse_and_eval("(- 3 2)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_INT(1, integer_value(result));
}


void test_subtract_many_args(void)
{
  data_t *result = parse_and_eval("(- 10 3 2 6 5 -3)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_INT(-3, integer_value(result));
}


void test_divide_zero_args(void)
{
  data_t *result = parse_and_eval("(/)", &err);
  TEST_ASSERT_NOT_NULL(err);
}


void test_divide_one_args(void)
{
  data_t *result = parse_and_eval("(/ 2)", &err);
  TEST_ASSERT_NOT_NULL(err);
}


void test_divide_two_args(void)
{
  data_t *result = parse_and_eval("(/ 6 2)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_INT(3, integer_value(result));
}


void test_divide_many_args(void)
{
  data_t *result = parse_and_eval("(/ 100 4 5)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_INT(5, integer_value(result));
}


void test_modulus_args(void)
{
  data_t *result = parse_and_eval("(% 6 2)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_INT(0, integer_value(result));
}


void test_modulus2_args(void)
{
  data_t *result = parse_and_eval("(% 5 2)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_INT(1, integer_value(result));
}


void test_modulus3_args(void)
{
  data_t *result = parse_and_eval("(% 8 3)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_INT(2, integer_value(result));
}


void test_abs_with_positive(void)
{
  data_t *result = parse_and_eval("(abs 3)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_INT(3, integer_value(result));
}


void test_abs_with_negative(void)
{
  data_t *result = parse_and_eval("(abs -3)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_INT(3, integer_value(result));
}


void test_abs_with_zero(void)
{
  data_t *result = parse_and_eval("(abs 0)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_INT(0, integer_value(result));
}


void test_zero_true(void)
{
  data_t *result = parse_and_eval("(zero? 0)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(BOOLEAN_TYPE, type_of(result));
  TEST_ASSERT_TRUE(boolean_value(result));
}  


void test_zero_false(void)
{
  data_t *result = parse_and_eval("(zero? 1)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(BOOLEAN_TYPE, type_of(result));
  TEST_ASSERT_FALSE(boolean_value(result));
}  
