/* Copyright 2015 Dave Astels.  All rights reserved. */
/* Use of this source code is governed by a BSD-style */
/* license that can be found in the LICENSE file. */

/* This package implements a basic LISP interpretor for the ARM Cortex M4 */
/* This file contains tests for primitives. */

#include <string.h>
#include "unity.h"
#include "parser.h"
#include "tokenizer.h"
#include "data.h"
#include "dictionary.h"
#include "hash.h"
#include "header.h"
#include "vector.h"
#include "utils.h"
#include "evaluator.h"
#include "environment_frame.h"
#include "environment_vector.h"
#include "primitive_function.h"
#include "function.h"
#include "special_forms.h"

void setUp(void)
{
  initialize_lisp_data_system();
  initialize_environment();
  register_special_forms();
}

data_t *test_func_impl(data_t *args, environment_frame_t *env, char **err_ptr)
{
  data_t *arg1 = car(args);
  data_t *arg2 = car(cdr(args));
  return integer_with_value(integer_value(arg1) + integer_value(arg2));
}


void test_registering_a_primitive(void)
{
  register_primitive("test-func", 2, &test_func_impl);

  data_t *prim_object = value_of(GLOBAL_ENV, intern_symbol("test-func"));
  TEST_ASSERT_NOT_NULL(prim_object);
  TEST_ASSERT_EQUAL_INT(PRIMITIVE_TYPE, type_of(prim_object));
  primitive_function_t *prim = prim_value(prim_object);
  TEST_ASSERT_EQUAL_STRING("test-func", prim->name);
}


void test_applying_basic_primitive(void)
{
  register_primitive("test-func", 2, &test_func_impl);
  char *err = NULL;
  data_t *args = parse("(1 2)", &err);
  data_t *prim_object = value_of(GLOBAL_ENV, intern_symbol("test-func"));
  primitive_function_t *prim = prim_value(prim_object);

  data_t *result = apply_prim(prim, args, GLOBAL_ENV, &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_INT(3, integer_value(result));
}


void test_evaling_basic_primitive(void)
{
  register_primitive("test-func", 2, &test_func_impl);
  char *err = NULL;
  data_t *sexpr = parse("(test-func 1 2)", &err);

  data_t *result = evaluate(sexpr, GLOBAL_ENV, &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_INT(3, integer_value(result));
}
