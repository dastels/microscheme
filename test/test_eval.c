/* Copyright 2015 Dave Astels.  All rights reserved. */
/* Use of this source code is governed by a BSD-style */
/* license that can be found in the LICENSE file. */

/* This package implements a basic LISP interpretor for the ARM Cortex M4 */
/* This file contains tests for evaluation. */

#include "unity.h"
#include "parser.h"
#include "tokenizer.h"
#include "data.h"
#include "dictionary.h"
#include "hash.h"
#include "header.h"
#include "vector.h"
#include "utils.h"
#include "primitive_function.h"
#include "environment_frame.h"
#include "environment_vector.h"
#include "evaluator.h"

void setUp(void)
{
  initialize_lisp_data_system();
  initialize_environment();
}

/* Simple data */

void test_evaluating_integer(void) {
  char *err = NULL;
  data_t *sexpr = integer_with_value(5);
  data_t *result = evaluate(sexpr, GLOBAL_ENV, &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_INT(5, integer_value(result));
}


void test_evaluating_boolean(void) {
  char *err = NULL;
  data_t *sexpr = boolean_with_value(true);
  data_t *result = evaluate(sexpr, GLOBAL_ENV, &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(BOOLEAN_TYPE, type_of(result));
  TEST_ASSERT_TRUE(boolean_value(result));
}


void test_evaluating_string(void) {
  char *err = NULL;
  data_t *sexpr = string_with_value("test");
  data_t *result = evaluate(sexpr, GLOBAL_ENV, &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(STRING_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_INT("test", string_value(result));
}


void test_evaluating_symbol(void) {
  char *err = NULL;
  data_t *sexpr = intern_symbol("test");
  bind(GLOBAL_ENV, sexpr, integer_with_value(5));
  data_t *result = evaluate(sexpr, GLOBAL_ENV, &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_INT(5, integer_value(result));
}

