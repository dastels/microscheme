/* Copyright 2015 Dave Astels.  All rights reserved. */
/* Use of this source code is governed by a BSD-style */
/* license that can be found in the LICENSE file. */

/* This package implements a basic LISP interpretor for the ARM Cortex M4 */
/* This file contains tests for functions. */

#include <string.h>
#include "unity.h"
#include "parser.h"
#include "tokenizer.h"
#include "data.h"
#include "dictionary.h"
#include "hash.h"
#include "header.h"
#include "vector.h"
#include "utils.h"
#include "evaluator.h"
#include "environment_frame.h"
#include "environment_vector.h"
#include "primitive_function.h"
#include "function.h"
#include "primitives.h"
#include "special_forms.h"

char *err = NULL;

void setUp(void)
{
  initialize_lisp_data_system();
  initialize_environment();
  register_special_forms();
  register_primitives();
}


void test_define_var(void)
{
  parse_and_eval("(define meaning 42)", &err);
  TEST_ASSERT_NULL(err);

  data_t *result = value_of(GLOBAL_ENV, intern_symbol("meaning"));
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_INT(42, integer_value(result));
}


void test_lambda(void)
{
  data_t *lambda = parse_and_eval("(lambda () 42)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(lambda);
  TEST_ASSERT_EQUAL_INT(FUNCTION_TYPE, type_of(lambda));
  data_t *result = apply_func(func_value(lambda), NULL, GLOBAL_ENV, &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_INT(42, integer_value(result));
}


void test_define_func(void)
{
  data_t *f = parse_and_eval("(define (meaning) 42)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(f);
  TEST_ASSERT_EQUAL_INT(FUNCTION_TYPE, type_of(f));

  data_t *result = parse_and_eval("(meaning)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_INT(42, integer_value(result));
}


void test_define_multi_line_func(void)
{
  data_t *f = parse_and_eval("(define (meaning) 1 2 42)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(f);
  TEST_ASSERT_EQUAL_INT(FUNCTION_TYPE, type_of(f));

  data_t *result = parse_and_eval("(meaning)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_INT(42, integer_value(result));
}


void test_define_func_with_arg(void)
{
  data_t *f = parse_and_eval("(define (meaning x) x)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(f);
  TEST_ASSERT_EQUAL_INT(FUNCTION_TYPE, type_of(f));

  data_t *result = parse_and_eval("(meaning 5)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_INT(5, integer_value(result));

  result = parse_and_eval("(meaning 42)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_INT(42, integer_value(result));
}

void test_cond_case_1(void)
{
  data_t *result = parse_and_eval("(cond (#t 42) (#t 7) (else 21))", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_INT(42, integer_value(result));
}


void test_cond_case_2(void)
{
  data_t *result = parse_and_eval("(cond (#f 42) (#t 7) (else 21))", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_INT(7, integer_value(result));
}


void test_cond_default_case(void)
{
  data_t *result = parse_and_eval("(cond (#f 42) (#f 7) (else 21))", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_INT(21, integer_value(result));
}


void test_cond_no_matching_case(void)
{
  data_t *result = parse_and_eval("(cond (#f 42) (#f 7))", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NULL(result);
}


void test_let(void)
{
  data_t *result = parse_and_eval("(let ((a 42) (b 1)) a)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_INT(42, integer_value(result)); 
}


void test_let_evaluation_scope(void)
{
  data_t *result = parse_and_eval("(let ((a 10)) (let ((a 42) (b a)) b))", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_INT(10, integer_value(result)); 
}


void test_let_star_evaluation_scope(void)
{
  data_t *result = parse_and_eval("(let ((a 10)) (let* ((a 42) (b a)) b))", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_INT(42, integer_value(result)); 
}


void test_quote(void)
{
  data_t *result = parse_and_eval("'(1 2)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_STRING("(1 2)", to_string(result));
}


void test_letrec_required(void)
{
  data_t *result = parse_and_eval("(let ((teven? (lambda (n) (if (zero? n) #t (todd? (- n 1))))) (todd? (lambda (n) (if (zero? n) #f (teven? (- n 1)))))) (teven? 88))", &err);
  TEST_ASSERT_NOT_NULL(err);
  TEST_ASSERT_NULL(result);
}


void test_letrec(void)
{
  data_t *result = parse_and_eval("(letrec ((teven? (lambda (n) (if (zero? n) #t (todd? (- n 1))))) (todd? (lambda (n) (if (zero? n) #f (teven? (- n 1)))))) (teven? 88))", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(BOOLEAN_TYPE, type_of(result));
  TEST_ASSERT_TRUE(boolean_value(result));
}


void test_internal_defines_using_lambdas(void)
{
  data_t *result = parse_and_eval("(let ((x 5)) (define foo (lambda (y) (bar x y))) (define bar (lambda (a b) (+ (* a b) a))) (foo (+ x 3)))", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_INT(45, integer_value(result));
}


void test_internal_defines_using_functions(void)
{
  data_t *result = parse_and_eval("(let ((x 5)) (define (foo y) (bar x y)) (define (bar a b) (+ (* a b) a)) (foo (+ x 3)))", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_INT(45, integer_value(result));
}


void test_letrec_instead_of_internal_defines(void)
{
  data_t *result = parse_and_eval("(let ((x 5)) (letrec ((foo (lambda (y) (bar x y))) (bar (lambda (a b) (+ (* a b) a)))) (foo (+ x 3))))", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_INT(45, integer_value(result));
}


void test_set_in_global_env(void)
{
  data_t *result = parse_and_eval_all("(define x 10) (set! x 42) x", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_INT(42, integer_value(result));
}


void test_set_in_let_env(void)
{
  data_t *result = parse_and_eval_all("(let ((x 10)) (set! x 42) x)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_INT(42, integer_value(result));
}


void test_set_in_let_env_stays_there(void)
{
  data_t *result = parse_and_eval_all("(define x 10) (let ((x 10)) (set! x 42) x) x", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_INT(10, integer_value(result));
}


void test_do_length(void)
{
  data_t *result = parse_and_eval("(do ((l '(1 2 3 4) (cdr l)) (c 0 (+ c 1))) ((nil? l) c))", &err);
  printf("%s\n", err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_INT(4, integer_value(result));
}
