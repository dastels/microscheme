/* Copyright 2015 Dave Astels.  All rights reserved. */
/* Use of this source code is governed by a BSD-style */
/* license that can be found in the LICENSE file. */

/* This package implements a basic LISP interpretor for the ARM Cortex M4 */
/* This file contains tests for the cons cell. */

#include "unity.h"
#include "parser.h"
#include "tokenizer.h"
#include "data.h"
#include "dictionary.h"
#include "hash.h"
#include "header.h"
#include "vector.h"
#include "evaluator.h"
#include "utils.h"
#include "environment_frame.h"
#include "environment_vector.h"
#include "function.h"
#include "primitive_function.h"


data_t *d1;
data_t *d2;
data_t *cell;

void setUp(void)
{
  initialize_lisp_data_system();
  d1 = integer_with_value(1);
  d2 = integer_with_value(2);
  cell = cons(d1, d2);
}

void test_car(void) 
{
  TEST_ASSERT_NOT_NULL(car(cell));
  TEST_ASSERT_EQUAL(d1, car(cell));
}


void test_nil_car(void) 
{
  TEST_ASSERT_NULL(car(NULL));
}


void test_cdr(void) 
{
  TEST_ASSERT_NOT_NULL(cdr(cell));
  TEST_ASSERT_EQUAL(d2, cdr(cell));
}
  

void test_nil_cdr(void) 
{
  TEST_ASSERT_NULL(cdr(NULL));
}


