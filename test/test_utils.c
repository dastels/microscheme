/* Copyright 2015 Dave Astels.  All rights reserved. */
/* Use of this source code is governed by a BSD-style */
/* license that can be found in the LICENSE file. */

/* This package implements a basic LISP interpretor for the ARM Cortex M4 */
/* This file contains tests for the utils. */

#include "unity.h"
#include "environment_frame.h"
#include "environment_vector.h"
#include "evaluator.h"
#include "tokenizer.h"
#include "parser.h"
#include "data.h"
#include "dictionary.h"
#include "hash.h"
#include "header.h"
#include "vector.h"
#include "utils.h"

char *err = NULL;

void setUp(void)
{
  initialize_lisp_data_system();
}


void test_quote_it(void)
{
  data_t *quoted = quote_it(integer_with_value(5));
  TEST_ASSERT_EQUAL_INT(CONS_CELL_TYPE, type_of(quoted));
  TEST_ASSERT_EQUAL_INT(2, length_of(quoted));
  TEST_ASSERT_EQUAL_INT(SYMBOL_TYPE, type_of(car(quoted)));
  TEST_ASSERT_EQUAL_STRING("quote", string_value(car(quoted)));
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(car(cdr(quoted))));
  TEST_ASSERT_EQUAL_INT(5, integer_value(car(cdr(quoted))));
}


void test_quote_all(void)
{
  data_t *sexpr = parse("(1 2 3)", &err);
  data_t *quoted = quote_all(sexpr);
  TEST_ASSERT_EQUAL_INT(CONS_CELL_TYPE, type_of(quoted));
  TEST_ASSERT_EQUAL_INT(3, length_of(quoted));

  data_t *first = car(quoted);
  TEST_ASSERT_EQUAL_INT(SYMBOL_TYPE, type_of(car(first)));
  TEST_ASSERT_EQUAL_STRING("quote", string_value(car(first)));
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(car(cdr(first))));
  TEST_ASSERT_EQUAL_INT(1, integer_value(car(cdr(first))));
  
  data_t *second = car(cdr(quoted));
  TEST_ASSERT_EQUAL_INT(SYMBOL_TYPE, type_of(car(second)));
  TEST_ASSERT_EQUAL_STRING("quote", string_value(car(second)));
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(car(cdr(second))));
  TEST_ASSERT_EQUAL_INT(2, integer_value(car(cdr(second))));
  
  data_t *third = car(cdr(cdr(quoted)));
  TEST_ASSERT_EQUAL_INT(SYMBOL_TYPE, type_of(car(third)));
  TEST_ASSERT_EQUAL_STRING("quote", string_value(car(third)));
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(car(cdr(third))));
  TEST_ASSERT_EQUAL_INT(3, integer_value(car(cdr(third))));
  
}


void test_flatten_all_atoms(void)
{
  data_t *result = flatten(parse("(1 2 3 4)", &err));
  TEST_ASSERT_EQUAL_STRING("(1 2 3 4)", to_string(result));
}


void test_flatten_with_lists(void)
{
  data_t *result = flatten(parse("(1 (2 3) 4)", &err));
  TEST_ASSERT_EQUAL_STRING("(1 2 3 4)", to_string(result));
}


void test_flatten_with_nested_lists(void)
{
  data_t *result = flatten(parse("(1 (2 (3 4) 5) 6)", &err));
  TEST_ASSERT_EQUAL_STRING("(1 2 (3 4) 5 6)", to_string(result));
}
