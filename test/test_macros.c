/* Copyright 2015 Dave Astels.  All rights reserved. */
/* Use of this source code is governed by a BSD-style */
/* license that can be found in the LICENSE file. */

/* This package implements a basic LISP interpretor for the ARM Cortex M4 */
/* This file contains tests for functions. */

#include <string.h>
#include "unity.h"
#include "parser.h"
#include "tokenizer.h"
#include "data.h"
#include "dictionary.h"
#include "hash.h"
#include "header.h"
#include "vector.h"
#include "utils.h"
#include "evaluator.h"
#include "environment_frame.h"
#include "environment_vector.h"
#include "primitive_function.h"
#include "function.h"
#include "primitives.h"
#include "special_forms.h"

char *err = NULL;

void setUp(void)
{
  initialize_lisp_data_system();
  initialize_environment();
  register_special_forms();
  register_primitives();
}


void test_no_unquoting(void)
{
  data_t *result = parse_and_eval("`(+ a 1)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_STRING("(+ a 1)", to_string(result));
}


void test_unquoting_integer(void)
{
  data_t *result = parse_and_eval("`(+ a ,1)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_STRING("(+ a 1)", to_string(result));
}


void test_unquoting_symbol(void)
{
  parse_and_eval("(define a 5)", &err);
  data_t *result = parse_and_eval("`(+ ,a 1)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_STRING("(+ 5 1)", to_string(result));
}


void test_unquoting_expression(void)
{
  parse_and_eval("(define a 5)", &err);
  data_t *result = parse_and_eval("`(+ ,(+ a 1) 1)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_STRING("(+ 6 1)", to_string(result));
}


void test_unquote_splicing(void)
{
  data_t *result = parse_and_eval("`(+ ,@(list 1 2 3) 1)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_STRING("(+ 1 2 3 1)", to_string(result));
}


void test_defmacro(void)
{
  data_t *m = parse_and_eval("(defmacro (square x) `(* ,x ,x))", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(m);
  TEST_ASSERT_EQUAL_INT(MACRO_TYPE, type_of(m));
}


void test_expand(void)
{
  parse_and_eval("(defmacro (square x) `(* ,x ,x))", &err);
  data_t *m = parse_and_eval("(expand square 5)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_EQUAL_STRING("(* 5 5)", to_string(m));
}



void test_invoke_macro(void)
{
  parse_and_eval("(defmacro (square x) `(* ,x ,x))", &err);
  data_t *result = parse_and_eval("(square 5)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_EQUAL_INT(INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_INT(25, integer_value(result));
}
