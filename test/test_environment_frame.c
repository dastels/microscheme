/* Copyright 2015 Dave Astels.  All rights reserved. */
/* Use of this source code is governed by a BSD-style */
/* license that can be found in the LICENSE file. */

/* This package implements a basic LISP interpretor for the ARM Cortex M4 */
/* This file contains tests for the environment frame. */

#include "unity.h"
#include "data.h"
#include "dictionary.h"
#include "vector.h"
#include "utils.h"
#include "hash.h"
#include "header.h"
#include "environment_vector.h"
#include "environment_frame.h"


environment_frame_t *frame;

void setUp(void)
{
  initialize_lisp_data_system();
  frame = new_environment_frame_below(NULL);
}

void test_local_binding(void)
{
  data_t *sym = symbol_with_name("test");
  bind(frame, sym, integer_with_value(42));

  data_t *value = value_of(frame, sym);
  TEST_ASSERT_NOT_NULL(value);
  TEST_ASSERT_EQUAL_INT(42, integer_value(value));
}

void test_no_local_binding(void)
{
  data_t *sym = symbol_with_name("test");
  data_t *value = value_of(frame, sym);
  TEST_ASSERT_NULL(value);
}

void test_inherited_binding(void)
{
  data_t *sym = symbol_with_name("test");
  bind(frame, sym, integer_with_value(42));

  environment_frame_t *subframe = new_environment_frame_below(frame);
  data_t *value = value_of(subframe, sym);
  TEST_ASSERT_NOT_NULL(value);
  TEST_ASSERT_EQUAL_INT(42, integer_value(value));
}

void test_no_inherited_binding(void)
{
  data_t *sym = symbol_with_name("test");
  environment_frame_t *subframe = new_environment_frame_below(frame);
  data_t *value = value_of(subframe, sym);
  TEST_ASSERT_NULL(value);
}


void test_interning(void)
{
  data_t *sym1 = intern_symbol("test");
  data_t *sym2 = intern_symbol("test");
  TEST_ASSERT_EQUAL_PTR(sym1, sym2);
}
