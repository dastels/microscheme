/* Copyright 2015 Dave Astels.  All rights reserved. */
/* Use of this source code is governed by a BSD-style */
/* license that can be found in the LICENSE file. */

/* This package implements a basic LISP interpretor for the ARM Cortex M4 */
/* This file contains tests for primitives. */

#include <string.h>
#include "unity.h"
#include "parser.h"
#include "tokenizer.h"
#include "data.h"
#include "dictionary.h"
#include "hash.h"
#include "header.h"
#include "vector.h"
#include "utils.h"
#include "evaluator.h"
#include "environment_frame.h"
#include "environment_vector.h"
#include "primitive_function.h"
#include "function.h"
#include "primitives.h"
#include "special_forms.h"

void setUp(void)
{
  initialize_lisp_data_system();
  initialize_environment();
  register_special_forms();
  register_primitives();
}


void test_binary_and(void)
{
  char *err;
  data_t *result = parse_and_eval("(binary-and #xaa #x0f)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(UNSIGNED_INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_INT(10, integer_value(result));
}


void test_binary_or(void)
{
  char *err;
  data_t *result = parse_and_eval("(binary-or #xaa #x014)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(UNSIGNED_INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_INT(0xBE, integer_value(result));
}


void test_binary_not(void)
{
  char *err;
  data_t *result = parse_and_eval("(binary-not #xaa)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(UNSIGNED_INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_UINT32(0xFFFFFF55, integer_value(result));
}


void test_binary_xor(void)
{
  char *err;
  data_t *result = parse_and_eval("(binary-xor #xaa #xf1)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(UNSIGNED_INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_UINT32(0x5B, integer_value(result));
}


void test_left_shift(void)
{
  char *err;
  data_t *result = parse_and_eval("(left-shift #x0a 3)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(UNSIGNED_INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_UINT32(0x50, integer_value(result));
}


void test_right_shift(void)
{
  char *err;
  data_t *result = parse_and_eval("(right-shift #x50 3)", &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT_NOT_NULL(result);
  TEST_ASSERT_EQUAL_INT(UNSIGNED_INTEGER_TYPE, type_of(result));
  TEST_ASSERT_EQUAL_UINT32(0x0A, integer_value(result));
}
