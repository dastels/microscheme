 /* Copyright 2015 Dave Astels.  All rights reserved. */
 /* Use of this source code is governed by a BSD-style */
 /* license that can be found in the LICENSE file. */

 /* This package implements a basic LISP interpretor for the ARM Cortex M4 */
 /* This file contains tests for the tokenizer. */

#include "unity.h"
#include "tokenizer.h"

void test_single_char_symbol(void) {
  initialize_tokenizer("a a");
  
  TEST_ASSERT_EQUAL(SYMBOL, get_token());
  TEST_ASSERT_EQUAL_STRING("a", get_lit());
}

void test_simple_multiple_character_symbol(void) {
  initialize_tokenizer("abc a");

  TEST_ASSERT_EQUAL(SYMBOL, get_token());
  TEST_ASSERT_EQUAL_STRING("abc", get_lit());
}

void test_symbol_with_digit(void) {
  initialize_tokenizer("ab123c a");

  TEST_ASSERT_EQUAL(SYMBOL, get_token());
  TEST_ASSERT_EQUAL_STRING("ab123c", get_lit());
}

void test_symbol_with_dashes(void) {
  initialize_tokenizer("abc-def a");
  
  TEST_ASSERT_EQUAL(SYMBOL, get_token());
  TEST_ASSERT_EQUAL_STRING("abc-def", get_lit());
}

void test_symbol_with_underscore(void) {
  initialize_tokenizer("abc_def a");
  
  TEST_ASSERT_EQUAL(SYMBOL, get_token());
  TEST_ASSERT_EQUAL_STRING("abc_def", get_lit());
}

void test_symbol_with_leading_underscore(void) {
  initialize_tokenizer("_abc_def a");
  
  TEST_ASSERT_EQUAL(SYMBOL, get_token());
  TEST_ASSERT_EQUAL_STRING("_abc_def", get_lit());
}

void test_symbol_with_question(void) {
  initialize_tokenizer("abc? a");
  
  TEST_ASSERT_EQUAL(SYMBOL, get_token());
  TEST_ASSERT_EQUAL_STRING("abc?", get_lit());
}

void test_symbol_with_bang(void) {
  initialize_tokenizer("abc! a");
  
  TEST_ASSERT_EQUAL(SYMBOL, get_token());
  TEST_ASSERT_EQUAL_STRING("abc!", get_lit());
}

void test_symbol_with_star(void) {
  initialize_tokenizer("abc* a");
  
  TEST_ASSERT_EQUAL(SYMBOL, get_token());
  TEST_ASSERT_EQUAL_STRING("abc*", get_lit());
}

void test_symbol_with_gt(void) {
  initialize_tokenizer("ab->c a");
  
  TEST_ASSERT_EQUAL(SYMBOL, get_token());
  TEST_ASSERT_EQUAL_STRING("ab->c", get_lit());
}

void test_string(void) {
  initialize_tokenizer("\"hi\" a");

  TEST_ASSERT_EQUAL(STRING, get_token());
  TEST_ASSERT_EQUAL_STRING("hi", get_lit());
}

void test_string_with_escaped_char(void) {
  initialize_tokenizer("\"hi\\\"\" a");

  TEST_ASSERT_EQUAL(STRING, get_token());
  TEST_ASSERT_EQUAL_STRING("hi\\\"", get_lit());
}

void test_quote(void) {
  initialize_tokenizer("'a");

  TEST_ASSERT_EQUAL(QUOTE, get_token());
  TEST_ASSERT_EQUAL_STRING("'", get_lit());
}

void test_backquote(void) {
  initialize_tokenizer("`a");

  TEST_ASSERT_EQUAL(BACKQUOTE, get_token());
  TEST_ASSERT_EQUAL_STRING("`", get_lit());
}

void test_comma(void) {
  initialize_tokenizer(",a");

  TEST_ASSERT_EQUAL(COMMA, get_token());
  TEST_ASSERT_EQUAL_STRING(",", get_lit());
}

void test_comma_at(void) {
  initialize_tokenizer(",@a");

  TEST_ASSERT_EQUAL(COMMAAT, get_token());
  TEST_ASSERT_EQUAL_STRING(",@", get_lit());
}

void test_lparen(void) {
  initialize_tokenizer("( a");

  TEST_ASSERT_EQUAL(LPAREN, get_token());
  TEST_ASSERT_EQUAL_STRING("(", get_lit());
}

void test_rparen(void) {
  initialize_tokenizer(") a");

  TEST_ASSERT_EQUAL(RPAREN, get_token());
  TEST_ASSERT_EQUAL_STRING(")", get_lit());
}

void test_lbracket(void) {
  initialize_tokenizer("[ a");

  TEST_ASSERT_EQUAL(LBRACKET, get_token());
  TEST_ASSERT_EQUAL_STRING("[", get_lit());
}

void test_rbracket(void) {
  initialize_tokenizer("] a");

  TEST_ASSERT_EQUAL(RBRACKET, get_token());
  TEST_ASSERT_EQUAL_STRING("]", get_lit());
}

void test_period(void) {
  initialize_tokenizer(". a");

  TEST_ASSERT_EQUAL(PERIOD, get_token());
  TEST_ASSERT_EQUAL_STRING(".", get_lit());
}

void test_add(void) {
  initialize_tokenizer("+ a");

  TEST_ASSERT_EQUAL(SYMBOL, get_token());
  TEST_ASSERT_EQUAL_STRING("+", get_lit());
}

void test_sub(void) {
  initialize_tokenizer("- a");

  TEST_ASSERT_EQUAL(SYMBOL, get_token());
  TEST_ASSERT_EQUAL_STRING("-", get_lit());
}

void test_mul(void) {
  initialize_tokenizer("* a");

  TEST_ASSERT_EQUAL(SYMBOL, get_token());
  TEST_ASSERT_EQUAL_STRING("*", get_lit());
}

void test_quo(void) {
  initialize_tokenizer("/ a");

  TEST_ASSERT_EQUAL(SYMBOL, get_token());
  TEST_ASSERT_EQUAL_STRING("/", get_lit());
}

void test_rem(void) {
  initialize_tokenizer("% a");

  TEST_ASSERT_EQUAL(SYMBOL, get_token());
  TEST_ASSERT_EQUAL_STRING("%", get_lit());
}

void test_leq(void) {
  initialize_tokenizer("<= a");

  TEST_ASSERT_EQUAL(SYMBOL, get_token());
  TEST_ASSERT_EQUAL_STRING("<=", get_lit());
}

void test_lss(void) {
  initialize_tokenizer("< a");

  TEST_ASSERT_EQUAL(SYMBOL, get_token());
  TEST_ASSERT_EQUAL_STRING("<", get_lit());
}

void test_geq(void) {
  initialize_tokenizer(">= a");

  TEST_ASSERT_EQUAL(SYMBOL, get_token());
  TEST_ASSERT_EQUAL_STRING(">=", get_lit());
}

void test_gtr(void) {
  initialize_tokenizer("> a");

  TEST_ASSERT_EQUAL(SYMBOL, get_token());
  TEST_ASSERT_EQUAL_STRING(">", get_lit());
}

void test_eql(void) {
  initialize_tokenizer("== a");

  TEST_ASSERT_EQUAL(SYMBOL, get_token());
  TEST_ASSERT_EQUAL_STRING("==", get_lit());
}

void test_double_equal(void) {
  initialize_tokenizer("== a");

  TEST_ASSERT_EQUAL(SYMBOL, get_token());
  TEST_ASSERT_EQUAL_STRING("==", get_lit());
}

void test_single_equal(void) {
  initialize_tokenizer("= a");

  TEST_ASSERT_EQUAL(SYMBOL, get_token());
  TEST_ASSERT_EQUAL_STRING("=", get_lit());
}

void test_neq(void) {
  initialize_tokenizer("!= a");

  TEST_ASSERT_EQUAL(SYMBOL, get_token());
  TEST_ASSERT_EQUAL_STRING("!=", get_lit());
}

void test_not(void) {
  initialize_tokenizer("! a");

  TEST_ASSERT_EQUAL(SYMBOL, get_token());
  TEST_ASSERT_EQUAL_STRING("!", get_lit());
}

void test_chain(void) {
  initialize_tokenizer("-> a");

  TEST_ASSERT_EQUAL(SYMBOL, get_token());
  TEST_ASSERT_EQUAL_STRING("->", get_lit());
}

void test_parallel_chain(void) {
  initialize_tokenizer("=> a");

  TEST_ASSERT_EQUAL(SYMBOL, get_token());
  TEST_ASSERT_EQUAL_STRING("=>", get_lit());
}

void test_false(void) {
  initialize_tokenizer("#f a");

  TEST_ASSERT_EQUAL(FALSE, get_token());
  TEST_ASSERT_EQUAL_STRING("#f", get_lit());
}

void test_true(void) {
  initialize_tokenizer("#t a");

  TEST_ASSERT_EQUAL(TRUE, get_token());
  TEST_ASSERT_EQUAL_STRING("#t", get_lit());
}

void test_short_number() {
  initialize_tokenizer("1 a");

  TEST_ASSERT_EQUAL(INTEGER, get_token());
      TEST_ASSERT_EQUAL_STRING("1", get_lit());
}

void test_long_integer() {
  initialize_tokenizer("1234567 a");

  TEST_ASSERT_EQUAL(INTEGER, get_token());
  TEST_ASSERT_EQUAL_STRING("1234567", get_lit());
}

void test_negative_integer() {
  initialize_tokenizer("-42 a");

  TEST_ASSERT_EQUAL(INTEGER, get_token());
  TEST_ASSERT_EQUAL_STRING("-42", get_lit());
}

void test_hex_integer() {
  initialize_tokenizer("#x1f a");

  TEST_ASSERT_EQUAL(HEXINTEGER, get_token());
  TEST_ASSERT_EQUAL_STRING("#x1f", get_lit());
}

void test_uppercase_hex_integer() {
  initialize_tokenizer("#x1F a");

  TEST_ASSERT_EQUAL(HEXINTEGER, get_token());
  TEST_ASSERT_EQUAL_STRING("#x1F", get_lit());
}
